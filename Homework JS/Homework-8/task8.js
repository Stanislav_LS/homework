let focus = document.getElementById('inputPrice');
    focus.addEventListener('focus', () =>{
    focus.classList.add('focus');
});

    focus.addEventListener('blur', () =>{
    focus.classList.add('blur');
    addElement();

});
//Validation error
function createError() {
    let newElement = document.createElement("span");
    newElement.innerHTML = `Please enter correct price`;
    focus.className = 'error';
    document.body.after(newElement);
}


//GET VALUE
function addElement() {
    const inputValue = document.getElementById("inputPrice").value;
    if (inputValue < 0) {
    createError();
    } else {
    createElem(inputValue);
    }

}
//Create elem
function createElem(value) {
    let newElement = document.createElement("span");
    newElement.innerHTML = `Текущая цена: ${value}`;
    newElement.className = 'list-item';
    document.body.before(newElement);
    createRemoveBtn();
}

//REMOVE BUTTON
function createRemoveBtn() {
    let newSpan = document.createElement("span");
    newSpan.innerHTML = "x";
    newSpan.className = "list-item-remove-btn list-item";
    newSpan.addEventListener('click', function (event){
        event.target.closest('.list-item').remove()
    })
    document.body.before(newSpan);
}