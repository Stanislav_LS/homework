let theme = document.querySelector('html');
document.getElementById('change').addEventListener('click', function (){
    if (localStorage.getItem('theme') === 'light' || (localStorage.getItem('theme') == null )) {
    theme.setAttribute(`data-theme`,'dark');
    localStorage.setItem("theme", "dark");
    } else {
        theme.setAttribute(`data-theme`,'light');
        localStorage.setItem("theme", "light");
    }
});

window.onload = function () {
    if (localStorage.getItem('theme') !== null) {
        let themeName = localStorage.getItem('theme');
        theme.setAttribute('data-theme', themeName);
    }
}