const data = ['hello', 'world', 23, '23', null];
function filterBy(array,dataType) {
    const filterArray = array.filter(element => (typeof element) !== dataType);
    return filterArray;

}
console.log(filterBy(data,'string'));

