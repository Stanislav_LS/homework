let userName = prompt('What is your name?');
while (!userName) {
    userName = prompt('What is your name?');
}
let userAge = +prompt('What is your age?');
while (Number.isNaN(+userAge) || userAge === "" || userAge === null) {
    userAge = +prompt('What is your age?');
}
if (userAge < 18) {
    alert('You are not allowed to visit this website');
} else if (userAge > 18 && userAge < 22) {
    answer = confirm('Are you sure you want to continue?');
    if (answer) {
        alert(`Welcome, ${userName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${userName}`);
}