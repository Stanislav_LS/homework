/*TABS SERVICES*/
document.querySelector(".tabs").addEventListener("click", (event) => {
    if (event.target.classList.contains("tabs-title")) {
        document.querySelector(".tabs .active").classList.remove("active");
        event.target.classList.add("active");
        document
            .querySelector(".tabs-content .active")
            .classList.remove("active");
        document
            .querySelector(
                `.tabs-content li[data-tab="${event.target.dataset.tab}"]`
            )
            .classList.add("active");
    }
});

/*WORK Tabs*/

const showAllLi = document.querySelector('.show-all');
const showWeb = document.querySelector('.web-design');
const showGrDes = document.querySelector('.gr-design');
const showWordpress = document.querySelector('.wordpress');
const showLp = document.querySelector('.lp')

const allLi = document.querySelectorAll('.filter-li');
const  arrayAllLi = [...allLi];
const allWork = document.querySelector('.work-tabs-title');
console.log(arrayAllLi);

showAllLi.addEventListener('click', ()=>{
    allWork.classList.remove('active');
    showAllLi.classList.add("active");
    arrayAllLi.forEach(li=>{
        li.classList.remove('remove');
        li.classList.add('active');
    })
})

showWeb.addEventListener('click', ()=>{
    allWork.classList.remove('active');
    showWeb.classList.add("active");
    arrayAllLi.forEach(li=>{
        li.classList.remove('remove');
        if(!li.dataset.web)
        li.classList.add('remove');
    })
})


showGrDes.addEventListener('click', ()=>{
    allWork.classList.remove('active');
    showGrDes.classList.add("active");
    arrayAllLi.forEach(li=>{
        li.classList.remove('remove');
        if(!li.dataset.gd)
            li.classList.add('remove');
    })
})

showLp.addEventListener('click', ()=>{
    allWork.classList.remove('active');
    showLp.classList.add("active");
    arrayAllLi.forEach(li=>{
        li.classList.remove('remove');
        if(!li.dataset.lp)
            li.classList.add('remove');
    })
})

showWordpress.addEventListener('click', ()=>{
    allWork.classList.remove('active');
    showWordpress.classList.add("active");
    arrayAllLi.forEach(li=>{
        li.classList.remove('remove');
        if(!li.dataset.wordpress)
            li.classList.add('remove');
    })
})

/*LOAD BTN*/

document.querySelector(".load-btn").addEventListener("click", (event) => {
    event.preventDefault();
    document.querySelector('.work-tabs-content.hidden').classList.remove('hidden');
    document.querySelector(".load-btn").classList.add('hidden');
});